const fyysisyysImages = ['https://i.imgur.com/HPiK6vw.png','https://i.imgur.com/tNQnJSZ.png','https://i.imgur.com/UsvKSy6.png','https://i.imgur.com/XooCKpn.png','https://i.imgur.com/Jm066HB.png']
const ajatteluImages = ['https://i.imgur.com/9u3Tn8U.png','https://i.imgur.com/PoEqOtt.png','https://i.imgur.com/yfGUVOE.png','https://i.imgur.com/sihwuZo.png','https://i.imgur.com/DewUuhM.png']
const sosiaalisuusImages = ['https://i.imgur.com/UQiuWcC.png','https://i.imgur.com/BhnLSEF.png','https://i.imgur.com/Dy55kAS.png','https://i.imgur.com/aT13XQw.png','https://i.imgur.com/3ad2k1h.png']

export const getSliderImg = (category,value) => {
    switch (category) {
        default:
            return 'https://image.shutterstock.com/image-vector/no-image-available-vector-illustration-260nw-744886198.jpg'
        case 0:
            return fyysisyysImages[value-1]
        case 1:
            return sosiaalisuusImages[value-1]
        case 2:
            return ajatteluImages[value-1]
    }
}