import React from 'react';

const organizationContext = React.createContext({
    username: '',
    name: '',
    email: '',
    phone: '',
    site: '',
    city: '',
    address: '',
    postcode: '',
    handleValueChange: () => {},
    handleChange: () => {},
    buttonPressedHandler: () => {},
    data: {}
});

export default organizationContext;