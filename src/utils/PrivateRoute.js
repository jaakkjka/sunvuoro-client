import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { checkRequiredCredentials } from './AuthUtils';

function PrivateRoute({ component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={(props) => (checkRequiredCredentials(rest.user) ? <Component {...props} /> : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />)}
    />
  );
}

export default PrivateRoute;
