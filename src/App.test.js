import React from 'react';
import { render } from '@testing-library/react';
import { axe } from 'jest-axe';
import App from './App';

it('App should not have basic accessibility issues', async () => {
  const { container } = render(<App />);
  const results = await axe(container);
  expect(results).toHaveNoViolations();
});
