import React from 'react';
import { render } from '@testing-library/react';
import { axe } from 'jest-axe';
import axios from 'axios';
import ApplicationsPage from './ApplicationsPage';

jest.mock('axios');

it('AdminPage should not have basic accessibility issues', async () => {
  axios.get.mockResolvedValue({

    data: {
      results: [
        {
          application_id: 0,
          organization_name: 'org name',
          organization_email: '',
          organization_phone: '',
          organization_link: '',
          organization_city: '',
          organization_address: '',
          organization_postcode: '',
          free_text: '',
          adminComment: ''
        }
      ]
    }

  });

  const { container } = render(<ApplicationsPage />);
  const results = await axe(container);
  expect(results).toHaveNoViolations();
});
