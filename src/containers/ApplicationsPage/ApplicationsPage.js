import React, { Component } from 'react';
import axios from 'axios';
import classes from '../ApplicationsPage/ApplicationsPage.module.css';
import {getApplicationRoute,getOrganizationRoute} from '../../utils/Api';
import { getToken } from '../../utils/AuthUtils';
import Application from '../../components/Application'
import Popup from '../../components/Popup/Popup'

class ApplicationsPage extends Component {
    state = {
        applicationData: [{
            application_id: 0,
            organization_name: '',
            organization_email: '',
            organization_phone: '',
            organization_link: '',
            organization_city: '',
            organization_address: '',
            organization_postcode: '',
            free_text: '',
            adminComment: ''
          }],
          alert: {
            showAlert: false,
            success:false,
            alertType: "",
            alertMessage: ""
          }
    }

    async componentDidMount() {
        await axios.get(getApplicationRoute()).then(response => {
            this.setState({applicationData: response.data.results})
          })
    }

    togglePopup = (alertType) => {
      const data = this.state.alert
      data.alertType = alertType;
      data.showAlert = !data.showAlert
      this.setState({
        alert: data
      })
      this.setAlertMessage(alertType)
      console.log(this.state.alert)
      if(!this.state.alert.showAlert) {
        window.location.reload();
}
    }

    setAlertMessage = (alertType) => {
      const data = this.state.alert
      switch(alertType) {
        case "success":
          data.success = true;
          data.alertMessage = `Hakemus hyväksytty!`
          break;
        case "error":
          data.success = false;
          data.alertMessage = "Hakemus hylätty!"
          break;
        default:
          data.alertMessage = "alertMessage not found"
      }
      this.setState({
        alert:data
      })
    }

    handleValueChangeForAdminComment = name => event => {
        const data = this.state.applicationData
        data[`${name}`] = event.target.value;
        this.setState({
          applicationData: data
        })
      }

      handleButtonPress = (type,id) => {
        switch(type) {
          case 'acceptOrg':
            this.acceptOrganization(id);
            break;
          case 'rejectOrg':
            this.rejectOrganization(id);
          break;
          default:
            console.log('Something went wrong');
        }
      }

      acceptOrganization = (id) => {
        const payload = {
          application_id: id,
          adminComment: this.state.applicationData.adminComment
        }
        axios.post(getOrganizationRoute(),payload, {
          headers: {
            token: getToken()
          }
        })
        .then(response => {console.log('result', response);this.togglePopup("success")})
        .catch(error => {console.log("error",error.response)
      })
      }
  
      rejectOrganization = (id) => {
        axios.delete(getApplicationRoute()+id)
        .then(response => {console.log(response);this.togglePopup("error")})
      }

    render() {
        const applications = this.state.applicationData.filter(({organization_name}) => organization_name).map(apply => {
            return(
              <li key={apply.application_id}>
                <Application
                  eventKey={apply.application_id}
                  orgName={apply.organization_name}
                  email={apply.organization_email}
                  phone={apply.organization_phone}
                  link={apply.organization_link}
                  address={apply.organization_address}
                  postcode={apply.organization_postcode}
                  city={apply.organization_city}
                  free_text={apply.free_text}
                  acceptButtonPressedHandler={() => this.handleButtonPress("acceptOrg",apply.application_id)}
                  rejectButtonPressedHandler={() => this.handleButtonPress("rejectOrg",apply.application_id)}
                  handleValueChange={this.handleValueChangeForAdminComment}
                />
              </li>
            )
          })
        return(
            <div className={classes.ApplicationsContainer}>
                {this.state.alert.showAlert
                  && <Popup
                      role="dialog"
                      className={this.state.alert.success ? "popup_inner_success" : "popup_inner_error"}
                      text={this.state.alert.alertMessage}
                      closePopup={this.togglePopup.bind(this)}
                    />}
                <div style={{ display: this.state.alert.showAlert ? 'block' : 'none' }} className="DialogOverlay" tabIndex="-1" />
                
                <div eventkey="applications" title="Hakemukset">
                  <h1 className={classes.OrgText}>
                    Käsittelyä odottavat organisaatiohakemukset
                  </h1>
                  <ul className={classes.ApplicationsList}>
                    {applications}
                  </ul>
                </div>
            </div>
        );
    }
}

export default ApplicationsPage;