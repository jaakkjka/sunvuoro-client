import React from 'react';
import { render } from '@testing-library/react';
import { axe } from 'jest-axe';
import QuizPage from './QuizPage';

it('QuizPage should not have basic accessibility issues', async () => {
  const { container } = render(<QuizPage />);
  const results = await axe(container, {
    rules: {
      // Issues exist in the rc-slider and multiselect-react-dropdown -libraries
      'aria-input-field-name': { enabled: false },
      'duplicate-id': { enabled: false }
    }
  });
  expect(results).toHaveNoViolations();
});
