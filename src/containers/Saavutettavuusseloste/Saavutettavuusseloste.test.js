import React from 'react';
import { render } from '@testing-library/react';
import { axe } from 'jest-axe';
import Saavutettavuusseloste from './Saavutettavuusseloste';

it('Saavutettavuusseloste should not have basic accessibility issues', async () => {
  const { container } = render(<Saavutettavuusseloste />);
  const results = await axe(container);
  expect(results).toHaveNoViolations();
});
