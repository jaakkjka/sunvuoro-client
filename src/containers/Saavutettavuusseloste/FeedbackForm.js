import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import './Saavutettavuusseloste.css';

const FeedbackForm = () => {
  const [validated, setValidated] = useState(false);

  const handleSubmit = event => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    setValidated(true);
  };

  return (
    <div className="feedback-container">
      <h1 className="main-title">Palautelomake</h1>
      <p>Tähän voit jättää meille palautetta sivustosta. Jos haluat, että sinuun ollaan mahdollisesti yhteydessä, jätä myös sähköpostiosoitteesi.</p>
      <Form noValidate validated={validated} onSubmit={handleSubmit} className="form-container">
        <Form.Group controlId="feedback-field">
          <Form.Label>Palaute</Form.Label>
          <Form.Control required as="textarea" rows="3" />
        </Form.Group>
        <Form.Group controlId="email-field">
          <Form.Label>Sähköposti (vapaaehtoinen)</Form.Label>
          <Form.Control type="text" />
        </Form.Group>
        <Button type="submit" variant="success">Lähetä palaute</Button>
      </Form>
    </div>
  );
};

export default FeedbackForm;
