import React, { Component, useState } from 'react';
import axios from 'axios';
import { getCitiesRoute, getDataCollectionRoute, getTaskRoute, getLocationsRoute, getOrganizationRoute } from '../../utils/Api';
import { Container, Row, Col, Button } from 'react-bootstrap';
import  { Doughnut, Polar, Bar } from 'react-chartjs-2';
import Loader from '../../components/Loader';
import { getUser } from '../../utils/AuthUtils';
import { Multiselect } from 'multiselect-react-dropdown';
import { PieChart } from 'react-minimal-pie-chart';
import Accordion from 'react-bootstrap/Accordion';
import { Card } from 'react-bootstrap';
import classes from '../AdminPage/AdminPage.module.css'
import DatePicker from 'react-datepicker';

class StatisticsPage extends Component {
    state = {
        cities: [],
        filteredCities:[],
        startDate: new Date("1, 1, 2000"),
        endDate: new Date(),
        filtersExpanded: 0,
        searchValuesRaw:[],
        filteredValues: [],
        filteredTasks: [],
        taskCount: 0,
        searchCount: 0,
        taskValuesRaw: [],
        contactValuesRaw: [],
        organizations: [],
        colors: [
            '#7BC5DC',
            '#CCDEE5',
            '#D6CB3A',
            '#D1D6A5',
            '#EA7659',
        ]
    }


    async componentDidMount() {
        await axios.get(getOrganizationRoute()+getUser().userId).then(response => {
            this.setState({organizations: response.data.results});
        })
        axios.get(getCitiesRoute()).then(response => {
            this.setState({cities: response.data.results, filteredCities: response.data.results})
        })
        getUser().isAdmin ?
        await axios.get(getTaskRoute()).then(response => {
            this.setState({taskValuesRaw: response.data.results, filteredTasks: response.data.results});
        }):
        await axios.get(getLocationsRoute()+this.state.organizations[0].location_id).then(response => {
            this.setState({taskValuesRaw: response.data.results,
            });
        })

        getUser().isAdmin ?
        await axios.get(getDataCollectionRoute()+'searchValues').then(response => {
            this.setState({searchValuesRaw: response.data.results,
                           filteredValues: response.data.results,
                           searchCount: response.data.results.length,
            });
        }) : this.setState(this.state);

    }
    handleCityChange = event => {
        const oldCities = [this.state.cities];
        const selectedCities = event.map(e => e);

        const changedCities = oldCities.map(city => ((selectedCities.includes(city))
                                                   ? { ...city, selected: true }
                                                   : { ...city, selected: false }));
        this.state.filteredCities = selectedCities;

    };

    // parametreina attribuutin nimi ja true, jos halutaan hakutuloksien tiedot, false, jos tarjolla olevien tehtävien tiedot.
    getAttributeValues = (attribute, searches) => {
        let values = [0, 0, 0, 0, 0] // nollataan 5 arvoa taulukkoon.
        let labelTexts = [];
        let title;
        let attributeData = searches ? this.state.filteredValues : this.state.filteredTasks;
        switch (attribute) {
            case 'fyysisyys':
                attributeData.forEach(val => {
                    values[val.fysiikka_value -1]++;
                    labelTexts =['1: vähän', '2', '3', '4', '5: paljon'];
                    title= 'Fyysisyys';
                }); break;
            case 'ajattelu':
                attributeData.forEach(val => {
                    values[val.ajattelu_value -1]++;
                    labelTexts =['1: vähän', '2', '3', '4', '5: paljon'];
                    title= 'Ajattelu';
                }); break;
            case 'sosiaalisuus':
                attributeData.forEach(val => {
                    values[val.ajattelu_value -1]++;
                    labelTexts =['1: vähän', '2', '3', '4', '5: paljon'];
                    title= 'Sosiaalisuus';
                }); break;
        }
        return (
            {labels: labelTexts,
             datasets: [
                 {
                     label: 'Hakujen määrä',
                     data: values,
                     backgroundColor: this.state.colors
             }],
        });
    }

    // Set doughnut style
    chartOptions = () => {
        return({
            display: true,
            circumference:  Math.PI,
            rotation: (Math.PI),
            cutoutPercentage: 50,
            legend: {
                position: 'bottom'
            },
            title: {
                display: true,
            }
        })
    }

    // Hakujen suodatusfunktio päivämäärän ja kaupungin mukaan.
    filterSearch = () => {
        let filtered = [];
        this.state.searchValuesRaw.forEach(val => {
            let cityfilter = false;
            this.state.filteredCities.forEach(city => {
                if (val[city.city_name] == 1) {
                    cityfilter = true;
                }
            })
            let valdate = new Date(val.Date);
            if (this.state.startDate <= valdate && this.state.endDate >= valdate && cityfilter ) {
                filtered.push(val);
            }
        });
        this.setState({filteredValues: filtered, searchCount: filtered.length});
    }

    filterTasks = () => {
        let filtered = [];
        this.state.taskValuesRaw.forEach(task => {
            let cityfilter = false;
            this.state.filteredCities.forEach(city => {
                if (task.city_name == city.city_name) {
                    cityfilter = true;
                }
            })
            let taskStart = new Date(task.startDate);
            let taskEnd = new Date(task.endDate)
            if (this.state.startDate <= taskEnd && this.state.endDate >= taskStart && cityfilter) {
                filtered.push(task);
            }
        });
        console.log("tehtäväfiltteri", filtered);
        this.setState({filteredTasks: filtered});
    }


   // Tämä funktio on toistaiseksi turha. Keskiarvoja ei pidetty järkevinä.
    getAverageValues = () => {
        let averages = {
            social: 0.0,
            physical: 0.0,
            problems: 0.0,
        }

        let count = 0
        this.state.searchValuesRaw.forEach(val => {
            averages.physical += val.fysiikka_value;
            averages.social += val.sosiaalisuus_value;
            averages.problems += val.ajattelu_value;
            count += 1;
        })

        averages.social = averages.social / count;
        averages.physical = averages.physical / count;
        averages.problems = averages.problems / count;

        return this.getDataset([averages.social.toFixed(2), averages.physical.toFixed(2), averages.problems.toFixed(2)],
                                this.state.colors,
                                'Keskiarvohaut',
                                ['Sosiaalisuus', 'Fyysisyys', 'Ongelmanratkonta']);
    }

    getTopTasksOnClicks = () => {
        const tasks = [...this.state.taskValuesRaw];
        tasks.sort((a, b) => (a.taskClicks < b.taskClicks) ? 1 : -1);
        const taskCount = 5;
        let tasksToShow = [];
        let taskLabels = [];
        for(let i = 0; i < taskCount; i++) {
            tasksToShow.push(tasks[i].taskClicks);
            taskLabels.push(tasks[i].name);
        }
        return this.getDataset(tasksToShow, this.state.colors, '', taskLabels);
    }

    getTopSearchedCities = () => {
        let city = this.state.cities;
        city.sort((a, b) => (a.searchCount < b.searchCount) ? 1 : -1);
        const cityCount = 5;
        let citiesToShow = [];
        let citiesLabels = [];
        if (city.length > 0) {
            for (let i = 0; i < cityCount; i++) {
                citiesToShow.push(city[i].searchCount);
                citiesLabels.push(city[i].city_name);
            }
        }
        return this.getDataset(citiesToShow, this.state.colors, '', citiesLabels);
    }

    getDataset = (vals, cols, label, labels) => {
        return {
            datasets: [{
                data: [...vals],
                backgroundColor: [...cols],
                label: label
            }],
            labels: [
                ...labels
            ],
        };
    }

    getOptions = (xAxisVisible, yAxisVisible, maintainAspectRatio) => {
        return {
            legend: {
                display: true
            },
            scales: {
                xAxes: [{
                    display: xAxisVisible,
                    ticks: {
                        min:0
                    }
                }],
                yAxes: [{
                    display: yAxisVisible,
                    ticks: {
                        min:0
                    }
                }],
            },
            maintainAspectRatio: maintainAspectRatio
        }
    }

    setStartDate = (date) => {
        this.setState({startDate: date});
    }

    setEndDate = (date) => {
        this.setState({endDate: date});
    }

    applyFilters = () => {
        console.log("hähäh")
        this.filterSearch();
        this.filterTasks();
    }

    render() {
        const filterContainer =
                <div>
                    <Accordion defaultActiveKey='0' style={{display: 'flex', flexDirection: 'column'}} >
                        <Card className={classes.filterCard}>
                                <Accordion.Toggle
                                    aria-expanded={this.state.filtersExpanded}
                                    onClick={() => this.setState({filtersExpanded: !this.state.filtersExpanded})}
                                    id='filters-1'
                                    aria-controls='section-1'
                                    className={classes.Accord}
                                    as={Card.Header}
                                    variant={Card.Header}
                                    eventKey='1'
                                >
                                    Suodata hakua
                            </Accordion.Toggle>
                            <Accordion.Collapse id='filters-1' aria-labelledby='filters-1' eventKey='1'>
                                <Card.Body >
                                    <Row className={classes.CardContainer}>
                                        <Col>
                                    <label>Aloituspäivämäärä</label>
                                    <DatePicker selected={this.state.startDate} onChange={date => this.setStartDate(date)}
                                                dateFormat="dd.MM.yyyy"/>
                                        </Col>
                                        <Col>
                                   <label>Lopetuspäivämäärä</label>
                                   <DatePicker selected={this.state.endDate} onChange={date => this.setEndDate(date)}
                                               dateFormat="dd.MM.yyyy"/>
                                        </Col>
                                        <Col className={classes.citySelect}>
                                        <label htmlFor="select-location" aria-label="Valitse sijainti. Kirjoita rajataksesi hakua." style={{ marginBottom: '0.1rem' }}>
                                            Valitse sijainti
                                        </label>
                                        <Multiselect
                                            className={classes.citySelect}
                                            options={this.state.cities}
                                            displayValue="city_name"
                                            placeholder="Valitse tai kirjoita"
                                            onSelect={this.handleCityChange}
                                            onRemove={this.handleCityChange}
                                            emptyRecordMsg="Dataa ei löytynyt"
                                            closeOnSelect={false}
                                            showCheckbox={false}
                                            closeIcon="close"
                                            avoidHighlightFirstOption={true}
                                        />
                                        </Col>
                                    </Row>
                                    <p>Tarjottuja tehtäviä yhteensä {this.state.taskValuesRaw.length} </p>
                                    <p>Tehtävähakuja hakuarvoilla {this.state.searchCount}</p>
                                    <button onClick={this.applyFilters}>Suodata</button>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                    </Accordion>
                </div>

        const doughnutCharts = this.state.searchValuesRaw.length > 0 ?
        <>
        <div className={classes.chartContainer}><h2> Fyysisyys</h2>
            <Row>
            <Col style={{width: '49%'}}>
            <h3>Haetut tehtävät</h3>
            <Doughnut data={this.getAttributeValues('fyysisyys', true)} options={this.chartOptions()} />
            </Col>
            <Col>
            <h3>Tarjotut tehtävät</h3>
            <Doughnut data={this.getAttributeValues('fyysisyys', false)} options={this.chartOptions()} />
            </Col>
            </Row>
        </div>
        <div className={classes.chartContainer}><h2> Sosiaalisuus</h2>
            <Row>
                <Col style={{width: '49%'}}>
                <h3>Haetut tehtävät</h3>
                <Doughnut data={this.getAttributeValues('sosiaalisuus', true)} options={this.chartOptions()} />
            </Col>
            <Col>
                <h3>Tarjotut tehtävät</h3>
                <Doughnut data={this.getAttributeValues('sosiaalisuus', false)} options={this.chartOptions()} />
            </Col>
        </Row>
        </div>
        <div className={classes.chartContainer}><h2> Ajattelu</h2>
            <Row>
                <Col style={{width: '49%'}}>
                    <h3>Haetut tehtävät</h3>
                <Doughnut data={this.getAttributeValues('ajattelu', true)} options={this.chartOptions()} />
            </Col>
            <Col>
                <h3>Tarjotut tehtävät</h3>
                <Doughnut data={this.getAttributeValues('ajattelu', false)} options={this.chartOptions()} />
            </Col>
        </Row>
        </div>
        </>
        : <Loader size={'sm'} />

        const DoughnutLayout = doughnutCharts;

        const searchValueRow = getUser().isAdmin ? DoughnutLayout : null;

        const clickedChart = this.state.taskValuesRaw.length > 0
        ? <Bar data={this.getTopTasksOnClicks()} width={250} height={250} responsive={true} options={this.getOptions(false, true, false)}/>
        : <Loader size={'sm'}/>

        const cityBar = <Bar data={this.getTopSearchedCities()} width={250} height={250} responsive={true} options={this.getOptions(false, true, false)} />

        const mostClickedRow = <Col style={{backgroundColor:'#fff', padding:'3rem'}}><Row style={{justifyContent:'center'}}>Klikatuimmat tehtävät</Row><Row>{clickedChart}</Row></Col>

        const mostSearchedCities = <Col className={classes.cityBar}>
            <Row style={{justifyContent: 'center'}}><h2 style={{textAlign: "center"}}>Haetuimmat Kaupungit</h2></Row><Row>{cityBar}</Row>
        </Col>

        return(
            <>
            <div className={classes.topDecor}></div>
        <div className={classes.bg}>
        <Container>
          <Row style={{ backgroundColor: 'white', display: 'flex', justifyContent: 'center', paddingTop: '2rem' }}>
            <h1>Tilastot</h1>
            </Row>
            {filterContainer}
            <Row style={{ backgroundColor: 'white', marginBottom: '3rem' }} md={2}>
                {searchValueRow}
                {mostClickedRow}
        </Row>
        </Container>
            </div>
            </>
        );
    }
}

export default StatisticsPage;
