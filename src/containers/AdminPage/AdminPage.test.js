import React from 'react';
import { render } from '@testing-library/react';
import { axe } from 'jest-axe';
import AdminPage from './AdminPage';
import AdminModal from './AdminModal';

describe('Components', () => {
  it('AdminPage should not have basic accessibility issues', async () => {
    const { container } = render(<AdminPage />);
    const results = await axe(container, {
      rules: {
        // Issue exists in the react-bootstrap -library
        'aria-allowed-role': { enabled: false }
      }
    });
    expect(results).toHaveNoViolations();
  });

  it('AdminModal should not have basic accessibility issues', async () => {
    const { container } = render(<AdminModal />);
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });
});
