import React from 'react';
import { render } from '@testing-library/react';
import { axe } from 'jest-axe';
import TasksPage from './TasksPage';

it('TasksPage should not have basic accessibility issues', async () => {
  const { container } = render(<TasksPage />);
  const results = await axe(container, {
    rules: {
      // Issue exists in the multiselect-react-dropdown -library
      'duplicate-id': { enabled: false }
    }
  });
  expect(results).toHaveNoViolations();
});
