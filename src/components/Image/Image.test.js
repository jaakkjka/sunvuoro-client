import React from 'react';
import { render } from '@testing-library/react';
import { axe } from 'jest-axe';
import Image from './Image';

it('Image should not have basic accessibility issues', async () => {
  const { container } = render(<Image imageAlt="alt text" />);
  const results = await axe(container);
  expect(results).toHaveNoViolations();
});
