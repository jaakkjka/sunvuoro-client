import React from 'react';
import Img from 'react-cool-img';
import classes from './Image.module.css';

// Suggest to use low quality or vector images
import loadingImage from '../../images/loading2.gif';
import errorImage from '../../images/error.jpg';

const Image = ({ image, imageAlt }) => (
  <Img
    placeholder={loadingImage}
    src={image}
    error={errorImage}
    debounce={100}
    cache={false}
    alt={imageAlt}
    className={classes.Image}
  />
);

export default Image;
