import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { hanldeFontSizeChange } from '../../utils/util';
import classes from './Footer.module.css';
import HVTLogo from '../../images/HVT_Logo_alt.png';
import HYLogo from '../../images/HY_Logo.png';
import LJLogo from '../../images/LJ_Logo.png';
import MPLogo from '../../images/MP_Logo.png';
import SLogo from '../../images/S_Logo.png';
import SJKLogo from '../../images/SJK_Logo.png';
import phoneicon from '../../images/mobil phone_EDIT.svg';
import emailicon from '../../images/envelope-solid.svg';
import VVLogo from '../../images/VVBW.png';
import EULogo from '../../images/EUBW.png';

const Footer = () => {
  return (
    <Row className={classes.FooterContainer}>
      <Col aria-label="Vaihda tekstin kokoa" className={classes.FontSizeContainer}>
        <button aria-label="Normaali tekstikoko" type="button" onClick={() => hanldeFontSizeChange('normal')} className={classes.FontSizeNormal}>A</button>
        <button aria-label="Suuri tekstikoko" type="button" onClick={() => hanldeFontSizeChange('medium')} className={classes.FontSizeMedium}>A</button>
        <button aria-label="Suurin tekstikoko" type="button" onClick={() => hanldeFontSizeChange('large')} className={classes.FontSizeLarge}>A</button>
      </Col>
      <Row className={classes.infoContainer}>
        <Col>
          <img className={classes.footerLogo} alt="Hyvinvoinnin tilat" src={HVTLogo} />
        </Col>
        <Col className={classes.MiniMenu}>
          <ul >
              <li><a href="/">Sun Vuoro</a></li>
              <li><a href="/quiz">Löydä Tehtäväsi</a></li>
              <li><a href="/tasks">Kaikki Tehtävät</a></li>
              <li><a href="/login">Tehtävien tarjoajalle</a></li>
           </ul>
        </Col>
        <Col className={classes.ContactInfo}>
          <p>Ota yhteyttä</p>
          <p><img src={phoneicon} /> 
              puh. 012 345 5678<br />
              <img src={emailicon} />
               esimerkki@esimerkki.com
          </p>
        </Col>
        <Col>
          <ul>
              <li><a href="https://www.metropolia.fi">Metropolia Ammattikorkeakoulu</a></li>
                  <li><a href="http://muotografia.fi/">Muotografia</a></li>
            <li className={classes.SaavutettavuusLinkContainer}>
            <a href="/saavutettavuusseloste" exact="true" className={classes.AccessibilityInfo}>Saavutettavuusseloste</a>
            </li>
        </ul>
        </Col>
      </Row>
      <Col m={12} className={classes.FooterCol}>
        <img className={classes.FooterImage} alt="Metropolia ammattikorkeakoulu" src={EULogo} />
        <img className={classes.FooterImage} alt="Metropolia ammattikorkeakoulu" src={VVLogo} />
        <img className={classes.FooterImage} alt="Metropolia ammattikorkeakoulu" src={MPLogo} />
        <img className={classes.FooterImage} alt="LAB University of Applied Sciences" src={SLogo} />
        <img className={classes.FooterImage} alt="Seinäjoen kansalaisopisto" src={SJKLogo} />
        <img className={classes.FooterImage} alt="Hyötykasviyhdistys" src={HYLogo} />
        <img className={classes.FooterImage} alt="Lapinjärvi" src={LJLogo} />
      </Col>
    </Row>
  );
};

export default Footer;
