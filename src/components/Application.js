import React, { useState } from 'react';
import Accordion from 'react-bootstrap/Accordion';
import { Button, Card } from 'react-bootstrap';
import { TextareaAutosize } from '@material-ui/core';
import classes from '../containers/ApplicationsPage/ApplicationsPage.module.css';

const Application = (props) => {
  const [accordionExpanded, setAccordionExpanded] = useState(false);
  return (
    <Accordion defaultActiveKey="0">
      <Card>
        <h2 className={classes.OrganizationToggleContainer}>
          <Accordion.Toggle
            aria-expanded={accordionExpanded}
            aria-controls="application-sect"
            id="application-label"
            className={classes.OrganizationTitle}
            as={Button}
            variant={Card.Header}
            eventKey={props.key}
            onClick={() => setAccordionExpanded(!accordionExpanded)}
          >
            {props.orgName}
          </Accordion.Toggle>
        </h2>
        <Accordion.Collapse id="application-sect" aria-labelledby="application-label" eventKey={props.key}>
          <Card.Body className={classes.SingleApplicationContainer}>
            <div className={classes.OrgInfo}>
              <div className={classes.HeaderContentContainer}>
                <h3 style={{paddingLeft:'0'}}>Organisaation tiedot</h3>
                <table>
                  <tbody>
                    <tr>
                      <th>Sähköposti</th>
                      <td>{props.email}</td>
                    </tr>
                    <tr>
                      <th>Puhelin</th>
                      <td>{props.phone}</td>
                    </tr>
                    <tr>
                      <th>Nettisivut</th>
                      <td>{props.link}</td>
                    </tr>
                    <tr>
                      <th>Osoite</th>
                      <td>{props.address}</td>
                    </tr>
                    <tr>
                      <th>Postinumero</th>
                      <td>{props.postcode}</td>
                    </tr>
                    <tr>
                      <th>Kaupunki</th>
                      <td>{props.city}</td>
                    </tr>
                    <tr>
                      <th>Muuta</th>
                      <td>{props.free_text}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className={classes.HeaderContentContainer}>
                <h3>Kirjoita kommentti hakijalle</h3>
                <TextareaAutosize
                  aria-label="Kirjoita kommentti hakijalle"
                  onBlur={props.handleValueChange('adminComment')}
                  style={{ display: 'flex', flex: '1', minHeight: '300px' }}
                  className={classes.OrgMessage}
                  cols={30}
                  rowsMax={1}
                  placeholder="Kirjoita kommentti hakijalle"
                />
              </div>
            </div>
            <div className={classes.ButtonContainer}>
              <Button type="button" variant="success" onClick={props.acceptButtonPressedHandler} className={classes.AcceptButton}>Hyväksy hakemus</Button>
              <Button type="button" variant="danger" onClick={props.rejectButtonPressedHandler} className={classes.DenyButton}>Hylkää hakemus</Button>
            </div>
          </Card.Body>
        </Accordion.Collapse>
      </Card>
    </Accordion>
  );
};

export default Application;
