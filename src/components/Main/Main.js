import React from 'react';
import { Link } from 'react-router-dom';
import {
  Button, Container, Row, Col
} from 'react-bootstrap';
import CookieConsent, { Cookies } from 'react-cookie-consent';
import classes from './Main.module.css';
import VVLogo from '../../images/VV_Logo.png';
import EULogo from '../../images/EU_Logo.png';
import HYTILogo from '../../images/HVT_Logo.png';
import InfoBubble from '../../images/info puhekupla.svg';


const Main = () => {
  return (
    <div className={classes.PageContainer}>
      <div id={classes.topBar} />
      <div className={classes.logoContainer}>
        <img className={classes.logo} alt="Hyvinvoinnin tilat-logo" src={HYTILogo} />
        <div className={classes.Flags}>
          <img className={classes.VipuVoimaaFlag} alt="Vipuvoimaa-logo" src={VVLogo} />
          <img className={classes.EuroopanUnioniFlag} alt="EU-logo" src={EULogo} />
        </div>
      </div>
      <Container className={classes.Container}>
        <Row className={classes.rowContainer}>
          <h1 className={classes.Header1}>Nyt on Sun vuoro</h1>
          <Col className={classes.Description1}>
              {/* <Row className={classes.AfterDescription} /> */}
            <h2 className={classes.Header2}>Mitä tekisi, missä tekisi, miten tekisi?</h2>
            {/* <Row className={classes.AfterDescription} /> */}
          </Col>
          <Col className={classes.ButtonContainer}><a tabIndex="-1" href="/quiz"><Button className={classes.Button1}>Löydä sun juttu!</Button></a></Col>
        </Row>
        <CookieConsent
        buttonText="OK"
        style={{ background: "#2B373B" }}>
          Käytämme evästeitä parantaaksemme sivuston käyttökokemusta. Hyväksyt evästeiden käytön jatkamalla sivun selaamista.
        </CookieConsent>
      </Container>
        <div className={classes.toolTipContainer}>
          <Row className={classes.Tooltip}>
            <img className={classes.TooltipImage} alt="" src={InfoBubble} />
            <ol className={classes.TooltipText}>
              <li>
                Onko sinulla aikaa, jonka voisit antaa yhteiselle tekemiselle? Etsitkö paikkaa, jossa suorittaa työkokeilua, työharjoittelua tai jossa osallistua vapaaehtoistoimintaan?
              </li>
              <li>
                Pohdi yksin tai yhdessä jonkun kanssa minkälainen tekeminen voisi hyödyttää sinua tällä hetkellä
              </li>
              <li>
                Klikkaa
                <b> Löydä sun juttu </b>
                -painiketta ja hae tehtäviä
              </li>
              <li>
                Voit myös selata kaikkia tehtäviä
                <b> Kaikki tehtävät</b>
                -välilehdeltä
              </li>
            </ol>
          </Row>
        </div>
    </div>
  );
};

export default Main;
