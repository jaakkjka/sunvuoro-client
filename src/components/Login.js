import React from 'react';
import { Button, Col } from 'react-bootstrap';
import classes from '../containers/ProfessionalPage/ProfessionalPage.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';

const Login = (props) => (
  <div className={classes.Login}>
      <h1 className={classes.header} >Kirjaudu sisään</h1>
    <form method="POST">
      <div className="form-group" style={{ marginTop: '1vh' }}>
        <label htmlFor="username-field">Käyttäjätunnus</label>
        <input
          id="username-field"
          type="text"
          name="username"
          className="form-control"
          value={props.username}
          onChange={props.usernameChange()}
          onKeyPress={(event) => {
            if (event.keyCode === 13) {
              props.login();
            }
          }}
          placeholder="Käyttäjätunnus"
        />
      </div>
      <div className="form-group">
        <label htmlFor="password-field">Salasana</label>
        <input
          id="password-field"
          type="password"
          name="password"
          className="form-control"
          value={props.password}
          onChange={props.passwordChange()}
          placeholder="Salasana"
        />
      </div>
      <Col className={classes.ButtonContainer}><Button className={classes.loginButton} onClick={props.login()}><FontAwesomeIcon icon={faArrowRight} /> Kirjaudu</Button></Col>
    </form>
    <p className={classes.regText}>Tai rekisteröidy uudeksi tilojen tarjoajaksi täyttämällä hakemus:</p>
    <Col className={classes.ButtonContainer}>
      <Button
        variant="success"
        className={classes.regButton}
        href="/register"
      ><FontAwesomeIcon icon={faArrowRight} /> Rekisteröidy
      </Button>
    </Col>
  </div>
);

export default Login;
