import React from 'react';
import { render } from '@testing-library/react';
import { axe } from 'jest-axe';
import Application from './Application';
import Loader from './Loader';
import Login from './Login';
import LocationMap from './Map';
import Router from './Router';

describe('Components', () => {
  it('Application should not have basic accessibility issues', async () => {
    const { container } = render(<Application handleValueChange={() => () => true} orgName="orgname" />);
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });

  it('Loader should not have basic accessibility issues', async () => {
    const { container } = render(<Loader />);
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });

  it('Login should not have basic accessibility issues', async () => {
    const { container } = render(<Login usernameChange={() => () => true} passwordChange={() => () => true} login={() => () => true} />);
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });

  it('LocationMap should not have basic accessibility issues', async () => {
    const { container } = render(<LocationMap />);
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });

  it('Router should not have basic accessibility issues', async () => {
    const { container } = render(<Router />);
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });
});
