import React from 'react';
import { render } from '@testing-library/react';
import { axe } from 'jest-axe';
import Category from './Category';

it('Category should not have basic accessibility issues', async () => {
  const { container } = render(<Category label="label" />);
  const results = await axe(container, {
    rules: {
      // Issue exists in the rc-slider -library
      'aria-input-field-name': { enabled: false }
    }
  });
  expect(results).toHaveNoViolations();
});
