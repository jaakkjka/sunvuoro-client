import React from 'react';
import { render } from '@testing-library/react';
import { axe } from 'jest-axe';
import Popup from './Popup';

it('Popup should not have basic accessibility issues', async () => {
  const { container } = render(<Popup />);
  const results = await axe(container);
  expect(results).toHaveNoViolations();
});
