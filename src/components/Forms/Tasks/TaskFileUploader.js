import React from 'react';
import Form from 'react-bootstrap/Form'
import classes from '../../../containers/AdminPage/AdminPage.module.css';
import TaskContext from '../../../utils/Context/TaskContext';

const FileUploader = () => {
  return (
    <TaskContext.Consumer>
      {context =>
        <Form.Group controlId="taskFileUploader" style={{ border: '1px solid grey', borderRadius:'5px', backgroundColor: '#fff', width: '80%' }}>
          <Form.Label>
            <p className={classes.Text}>
              Lisää kuva painamalla &quot;Choose File&quot;-painiketta (Tuetut kuvamuodot: JPG, JPEG, PNG)
            </p>
            <p className={classes.Text} style={{ color: '#ea1818', fontWeight: 'bold', marginBottom: '0' }}>
              HUOM: Videoita ei tällä hetkellä tueta
            </p>
          </Form.Label>
          <div className={classes.Text}>
            <Form.Control accept="image/*" type="file" onChange={context.fileSelectedHandler} />
          </div>
        </Form.Group>}
    </TaskContext.Consumer>);
};

export default FileUploader;
