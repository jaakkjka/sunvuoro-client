import React, { useState } from 'react';
import MultiSelect from 'react-multi-select-component';

const WorkTimeSelector = ({ options, onChange }) => {
  const workTimes = options.map(time => {
    return { label: time.worktime, value: time.worktime_id };
  });


  // let [selected, setSelected] = useState([]);

  return (
    <div style={{ width: '57vh', height: '30vh' }}>
      <pre>as</pre>
      <MultiSelect
        disableSearch={true}
        selectAllLabel="Valitse kaikki"
        options={workTimes}
        // value={selected}
        onChange={onChange}
        labelledBy="Valise tehtävän ajankohta"
      />
    </div>
  );
};

export default WorkTimeSelector;
