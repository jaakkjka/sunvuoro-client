import React from 'react';
import { Form, Row } from 'react-bootstrap';
import TaskContext from '../../../utils/Context/TaskContext';
import classes from '../../../containers/AdminPage/AdminPage.module.css';
import classes2 from './Tasks.module.css';

const TaskValueSelector = ({ selectorClass, title, screenReader, val, id }) => {
  const options = [1, 2, 3, 4, 5];

  options.map(option => {
    return <option>{option}</option>;
  });

  return (
    <Form.Group controlId={`${title}taskValueSelector`} aria-label={title} className={selectorClass}>
      <Form.Label className={classes.TaskValueForm}>
        <Row className={classes.TaskValueForm}>
          <span aria-hidden="true">{title}</span>
        </Row>
        <p className={classes2.screenreader}>{screenReader}</p>
        <Row className={classes.TaskValueForm}>
          <div className={classes.speechbubble}>
            <p className={classes2.screenreader}>{`Arvo: ${val}`}</p>
            <p className={classes2.TaskValue} aria-hidden="true">{val}</p>
          </div>
        </Row>
      </Form.Label>
      <TaskContext.Consumer>
        {context =>
          <Form.Control
            style={{ marginTop: '.5rem' }}
            className={classes.Slider}
            type="range"
            min="1"
            max="5"
            step="1"
            defaultValue={val}
            onChange={context.handleValueChange('task', id, context.data)}
          />}
      </TaskContext.Consumer>
    </Form.Group>
  );
};

export default TaskValueSelector;
