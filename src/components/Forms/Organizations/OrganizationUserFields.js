import React from 'react';
import Form from 'react-bootstrap/Form';
import classes from '../../../containers/AdminPage/AdminPage.module.css';

const userFields = (props) => {
return (
        <div>
            <p className={classes.Credential}>Organisaation käyttäjätunnus sisäänkirjautumista varten:</p>
            <Form.Group controlId="formBasicEmail" style={{display:'flex'}}>
            <Form.Control  className={classes.CredentialForm} type="text" placeholder='Uusi käyttäjätunnus' onChange={props.handleValueChange("organization_username")}/>
            </Form.Group>
        </div>
)
}

export default userFields;
