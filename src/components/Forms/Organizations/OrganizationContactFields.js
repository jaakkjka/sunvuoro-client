import React from 'react';
import Form from 'react-bootstrap/Form';
import {Row} from 'react-bootstrap';
//

import classes from '../../../containers/AdminPage/AdminPage.module.css';
const contactFields = (props) => {
    return(
        <div>
            <p className={classes.OrgContactTitle}>Organisaation yhteystiedot:</p>
            <Form.Group style={{display:'flex'}}>
                <Row>
                <Form.Control className={classes.Orgdata} type="text" placeholder='Organisaation nimi' onChange={props.handleValueChange('organization_name')}/>
                <Form.Control className={classes.Orgdata} type="email" placeholder='Organisaation sähköposti' onChange={props.handleValueChange('organization_email')}/>
                </Row>
                <Row>
                <Form.Control className={classes.Orgdata} type="text" placeholder='Organisaation puhelinnumero' onChange={props.handleValueChange('organization_phone')}/>
                <Form.Control className={classes.Orgdata} type="text" placeholder='Organisaation verkkosivut' onChange={props.handleValueChange('organization_link')}/>
                </Row>
            </Form.Group>
            <Form.Group>
            <Row>
            <Form.Control className={classes.Address} type="text" placeholder='Katuosoite' onChange={props.handleValueChange('organization_address')}/>
            </Row>
            <Row>
            <Form.Control className={classes.Postalcode} type="text" placeholder='Postinumero' onChange={props.handleValueChange('organization_postcode')}/>
            <Form.Control className={classes.City} type="text" placeholder='Kaupunki' onChange={props.handleValueChange('organization_city')}/>
            </Row>
            <Row>
            <Form.Control className={classes.Orgdata} type="text" placeholder='Vapaa teksti' onChange={props.handleValueChange('free_text')}/>
            </Row>
            </Form.Group>
        </div>
    );
}

export default contactFields;
