import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import Router from './components/Router';

const title = 'SunVuoro';

const App = () => {
  useEffect(() => {
    const { localStorage } = window;
    const fontSize = JSON.parse(localStorage.getItem('fontSize'));
    const html = document.querySelector(':root');
    html.style.fontSize = fontSize;
  }, []);

  return (
    <>
      <Helmet>
        <title>{ title }</title>
      </Helmet>
      <Router />
    </>
  );
};

export default App;
